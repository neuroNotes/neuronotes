import { DescriptionModel } from "./description-model";
import { NodeModel } from "./node-model";
import { PositionModel } from "./position-model";

export interface NodeToDescConnection {
    one: PositionModel;
    desc: DescriptionModel;
    node: NodeModel;
    two: PositionModel;
}