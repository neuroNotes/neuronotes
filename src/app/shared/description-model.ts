import { ImageModel } from "./image-model";
import { NodeModel } from "./node-model";
import { ObjType } from "./obj-type";
import { PositionModel } from "./position-model";
import { ImageService } from "./services/image.service";
import { VisibleObject } from "./visible-object";

export class DescriptionModel extends VisibleObject {
    text: string;
    isNested: boolean = true;
    image: ImageModel | null = null;

    constructor(text:string = '', id: number, visible: boolean, isNested: boolean, position: PositionModel, image: ImageModel | null = null){
        super(id, position);
        this.text = text;
        this.isNested = isNested;
        this.image = image,
        this.type = ObjType.Description;
    }

    getChildrens(first = false): VisibleObject[]{
            return [];
    }
}
