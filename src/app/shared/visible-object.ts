import { ObjType } from "./obj-type";
import { PositionModel } from "./position-model";

export class VisibleObject {
    id: number;
    position: PositionModel;
    type: ObjType = ObjType.none;
    used = false;

    constructor(id: number, position: PositionModel) {
        this.id = id;
        this.position = position;
    }

    getChildrens(first = false): VisibleObject[]{
        return [];
    }
}
