import { DescriptionModel } from "./description-model";
import { ObjType } from "./obj-type";
import { PositionModel } from "./position-model";
import { VisibleObject } from "./visible-object";

export class NodeModel extends VisibleObject{
    name: string;
    descriptions: DescriptionModel[] = [];
    connections: {obj: NodeModel, text?: string}[] = [];

    constructor(name: string, id: number, visible: boolean, position: PositionModel){
        super(id, position);
        this.name = name;
        this.type = ObjType.Node;
    }

    getChildrens(first = false): VisibleObject[]{
        if(!this.used){
            this.used = true;
            const childrens = [];
            childrens.push(...this.descriptions);
            const conns: VisibleObject[] = [];
            this.connections.forEach(x => conns.push(x.obj));
            childrens.push(...conns);
            conns.forEach(obj => childrens.push(...obj.getChildrens()));
            this.descriptions.forEach(obj => childrens.push(...obj.getChildrens()));
            if(first){
                this.used = false;
                childrens.forEach(obj => obj.used = false);
            }
            return [... new Set(childrens)];
        }
        else
            return [];
    }
}
