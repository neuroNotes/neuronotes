import { NodeModel } from "./node-model";
import { PositionModel } from "./position-model";

export interface NodeToNodeConnection {
    one: PositionModel; 
    two: PositionModel; 
    connText: string; 
    oneObj: NodeModel; 
    twoObj: NodeModel
}