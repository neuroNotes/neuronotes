export interface ImageModel {
    image: File;
    id: number;
}
