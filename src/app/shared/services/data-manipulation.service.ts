import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BehaviorSubject, merge, Observable, zipWith } from 'rxjs';
import { map, tap, zip } from 'rxjs/operators';
import { environment } from 'src/environments/environment';
import { DescriptionModel } from '../description-model';
import { DescriptionDTO } from '../DTO/description-dto';
import { ImageDTO } from '../DTO/image-dto';
import { NodeDTO } from '../DTO/node-dto';
import { ImageModel } from '../image-model';
import { NodeModel } from '../node-model';
import { ObjType } from '../obj-type';
import { VisibleObject } from '../visible-object';
import { VisibleObjectManipulationService } from './visible-object-manipulation.service';

@Injectable({
  providedIn: 'root'
})
export class DataManipulationService {

  constructor(private visibleObjectManipulationService: VisibleObjectManipulationService, private http: HttpClient) { }

  getPath(...path: (string | number)[]){
    let string = environment.apiLink;
    path.forEach(x => string += '/' + x)
    return string;
  }

  saveVisibleObjects(): Observable<any>{
    return merge(this.saveNodes(), this.saveDesc(), this.saveVaribles());
  }

  saveNodes(): Observable<any>{
    const mapedNodes: NodeDTO[] = this.mapNodes();
    console.log(mapedNodes);
    return this.http.post(this.getPath(environment.apiSaveNodes), mapedNodes);
  }

  saveDesc(): Observable<any>{
    const maped = this.mapNodesAndImage();
    console.log(maped);

    let headers = new HttpHeaders();
    /** In Angular 5, including the header Content-Type can invalidate your request */
    headers.append('Content-Type', 'multipart/form-data');
    headers.append('Accept', 'application/json');
    headers.append('enctype', 'multipart/form-data');

    if(maped.imgs.length > 0)
      this.uploadImages(maped.imgs);  

    return this.http.post(this.getPath(environment.apiSaveDesc), maped.desc);
  }

  uploadImages(imgs: ImageDTO[]){
    let headers = new HttpHeaders();
    headers.append('Content-Type', 'multipart/form-data');
    headers.append('Accept', 'application/json');
    
    imgs.forEach(img => {
      let file: File = img.file;
      let formData: FormData = new FormData();
      formData.append('uploadFile', file, file.name);
      formData.append('id', img.id+'');


      this.http.post(this.getPath(environment.apiSaveImage), formData, {headers: headers})
          .subscribe({
            next: (data: any) => console.log('success upload'+ img.file.name),
            error: (error: any) => console.error(error)}
          )
    });
  }

  saveVaribles() {
    const lastId = this.visibleObjectManipulationService._Id;
    return this.http.post(this.getPath(environment.apiSaveVar), {lastId: lastId});
  }

  mapNodes(index?: number): NodeDTO[]{
    const vo =  this.visibleObjectManipulationService.getAllVisibleObject(index);
    const nodes: NodeDTO[] = [];
    vo.forEach(obj => {
      if(obj.type === ObjType.Node){
        const node = obj as NodeModel;
        const mapedNode: NodeDTO = {
          id: node.id, name: node.name, type: node.type, position: node.position,
          descriptionsIds: node.descriptions.map(desc => desc.id),
          connectionsIds: node.connections.map(conn => {return {obj: conn.obj.id, text: conn.text}})
        };
        nodes.push(mapedNode);
      }
    })
    return nodes;
  }

  mapNodesAndImage(index?: number): {imgs: ImageDTO[], desc: DescriptionDTO[]}{
    const vo = this.visibleObjectManipulationService.getAllVisibleObject(index);
    const desces: DescriptionDTO[] = [];
    const images: ImageDTO[] = [];

    vo.forEach(obj => {
      if(obj.type === ObjType.Description){
        const desc = obj as DescriptionModel;
        const mapedDesc: DescriptionDTO = {
          text: desc.text,
          isNested: desc.isNested,
          imageId: desc && desc.image && desc.image.id !== null ? desc?.image.id : -1,
          id: desc.id,
          position: desc.position,
          type: desc.type
        };
        desces.push(mapedDesc);
        if(mapedDesc.imageId > -1 && desc.image){
          const mapedImage: ImageDTO = {
            file: desc.image.image,
            id: desc.image.id
          };
          images.push(mapedImage);
        }
      }
    })

    return {imgs: images, desc: desces}
  }

  loading: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);
  isLoading():Observable<boolean>{
    return this.loading.asObservable()
  }

  loadAll(){
    let nodes: NodeModel[] = [];
    let nodesDTO: NodeDTO[] = [];
    this.loading.next(true);
    this.getNodes().subscribe(array => {nodes.push(
      ...array.map(dto =>
        {
          const node = new NodeModel( 
            dto.name,
            dto.id,
            true,
            dto.position
          );
          node.connections.push()
          return node;
        }
      )); 
      nodesDTO.push(...array);  
      nodes = this.remapNodes(nodes, nodesDTO);
      this.getDesc().subscribe(descesDTO => {
        descesDTO.forEach(desc => {
          console.log("desc", desc);
          nodesDTO.forEach(nodeDto => {
            if (nodeDto.descriptionsIds.indexOf(desc.id) > -1) {
             nodes.find((node)=>node.id === nodeDto.id)?.descriptions.push(this.remapDesc(desc))
            }
          })
        });
        this.visibleObjectManipulationService.refreshNodes(nodes);
      });

    });

    // this.getImage(0).subscribe(console.log);
    this.getLastId().subscribe( x => {console.log('id', x); this.visibleObjectManipulationService._Id = x});
  }

  remapNodes(nodes: NodeModel[], nodesDTO: NodeDTO[]): NodeModel[]{
    console.log('nodes',nodes);
    console.log('nodesDto', nodesDTO);

    nodes.forEach(node =>{
      const dto = nodesDTO.find(dto => dto.id == node.id);
      console.log('dto',dto);
      dto?.connectionsIds.forEach(conn => {
        const connNode = nodes.find(connNode => connNode.id == conn.obj);
        if(connNode)
          node.connections.push({obj: connNode, text: conn.text});
      })
    });
    return nodes;
  }
  remapDesc(dto: DescriptionDTO): DescriptionModel{
    if(dto.imageId == -1)
      return new DescriptionModel(dto.text, dto.id, true, dto.isNested, dto.position, null)
    else
    {
      let des = new DescriptionModel(dto.text, dto.id, true, dto.isNested, dto.position, null);
      this.getImage(dto.imageId).subscribe(image =>{
        des.image = {id: dto.imageId, image: image}
      });
      return des;
    }
  }
  getNodes(): Observable<NodeDTO[]>{
    return this.http.get<NodeDTO[]>(this.getPath(environment.apiLoadNodes))
  }

  getDesc(): Observable<DescriptionDTO[]>{
    return this.http.get<DescriptionDTO[]>(this.getPath(environment.apiLoadDescription))
  }
  getImage(imageId: number): Observable<File> {
    return this.http.get(this.getPath(environment.apiLoadImage, imageId), { responseType: 'blob' }).pipe(map(blob => new File([blob], imageId+'_Image')));
  }
  getLastId(): Observable<number> {
    return this.http.get<{LastId: number}>(this.getPath(environment.apiLoadVar)).pipe(tap(console.log), map(x => x.LastId))
  }
}


