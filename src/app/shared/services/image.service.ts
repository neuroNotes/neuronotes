import { Injectable } from '@angular/core';
import { ImageModel } from '../image-model';

@Injectable({
  providedIn: 'root'
})
export class ImageService {

  constructor() {}

  _id: number = 0;
  get id(){
    return this._id++;
  }

  makeImage(img: File | null): ImageModel | null{
    if(img == null)
      return null
    else
      return {image: img, id: this.id}
  }
}
