import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { DescriptionModel } from '../description-model';
import { NodeModel } from '../node-model';
import { ObjType } from '../obj-type';
import { VisibleObject } from '../visible-object';

import { tap } from 'rxjs/operators';


@Injectable({
  providedIn: 'root'
})
export class VisibleObjectManipulationService {

  // VisibleObjects: NodeModel[] = [
  //   new NodeModel("Main", [], [], 0, 400, true, {left: 500, top: 500})
  // ];

  activeId = new BehaviorSubject<number>(0);
  _secondSelect = false;

  visibleObjects = new BehaviorSubject<NodeModel[][]>([
    [new NodeModel("Main", 0, true, {left: 500, top: 500})]
  ]);
  _secondSelectListener = new BehaviorSubject<any>({});

  _Id = 1;

  constructor() { }
  
  /** Get Id and automate increse it
  */
  get nextId(){
    return this._Id++;
  }

  /** Get Main Nodes
   * @param index index of node to get
   * @return Main nodes of index
   */
  getNodes(index: number): NodeModel[] | undefined {
    if(this.visibleObjects.getValue()[index])
      return this.visibleObjects.getValue()[index];
    return undefined;
  }
  /** Return all visibleObject in index
   */
  getAllVisibleObject(main_nodeIndex = 0): VisibleObject[] {
    const VisibleObjects = this.visibleObjects.getValue();
    const nodes : VisibleObject[] = [];
    if(VisibleObjects[main_nodeIndex])
      VisibleObjects[main_nodeIndex].forEach(node => nodes.push(... node.getChildrens(true), node));

    console.log(nodes, VisibleObjects);
    return [... new Set(nodes)];
  }

  /** Add node to parent node or desctription
   * @param newNode node to add
   * @param parentId id of parent visible object
   */
  addNode(newNode: NodeModel, parentId : number, main_nodeIndex: number = 0){
    const nodes = this.getAllVisibleObject(main_nodeIndex);

    if(parentId === -1)
      return
    
    nodes.forEach(node => {
      if(node.id === parentId){
        if(node.type === ObjType.Node){
          const noode = node as NodeModel;
          noode.connections.push({obj: newNode});
        }
        else if(node.type === ObjType.Description){
          this.addNodeWithoutParent(newNode);
          const desc = node as DescriptionModel;
          newNode.descriptions.push(desc);
        }
      }
    });
    this.refresh();
  }

  /** Adding node to main nodes without parent
   * 
   */
  addNodeWithoutParent(node: NodeModel, main_nodeIndex = 0){
    const VisibleObjects = this.visibleObjects.getValue();
    VisibleObjects[main_nodeIndex].push(node);
    this.refresh();
  }

  /** Add Node to active node or desctription
   * @param nodee node to add
   */
  addNodeToActive(nodee: NodeModel, main_nodeIndex: number = 0){
    let activeId = this.activeId.getValue();

    if(activeId > -1)
      this.addNode(nodee, activeId, main_nodeIndex);
    else
      this.addNodeWithoutParent(nodee, main_nodeIndex);
  }

  /** Add Desctription to active node
   * @param nodee Desctription to add
   */
  addDescToActive(nodee: DescriptionModel, main_nodeIndex: number = 0){
    this.addDesc(nodee, this.activeId.getValue(), main_nodeIndex);

  }

  /** Adding Desctription to node
   * 
   * @param nodee Desctription
   * @param parentId Parent Node Id
   * @param main_nodeIndex 
   */
  addDesc(nodee: DescriptionModel, parentId : number, main_nodeIndex: number = 0){
    if(parentId === -1)
      return

    const nodes = this.getAllVisibleObject(main_nodeIndex);
    nodes.forEach(node => {
      if(node.id === parentId){
        if(node.type === ObjType.Node){
          const noode = node as NodeModel;
          noode.descriptions.push(nodee);
        }
      }
    });
    this.refresh();
  }

  /** Change active to id or select second
   * @param child child instance
   * 
   */
  changeActiveId(id: number, vO: VisibleObject | undefined){
    if(!this._secondSelect)
      this.activeId.next(id);
    else if(vO){
      this._secondSelectListener.next(vO);
    }
  }

  /** Refresh visible objects
   * 
   */
  refresh(){
    this.visibleObjects.next(this.visibleObjects.getValue());
  }

  /** Next active will be second select
   */
  secondSelect(){
    this._secondSelect = true;
  }

  /** Get observable with second select
   * @return Second selected VisibleObject
   */
  getSecondSelect(): Observable<VisibleObject>{
    return this._secondSelectListener.pipe(tap((x) =>this._secondSelect = false));
  }

  /** Adding text to connection active to node
   * @param node parent instance
   * @param text string 
   */
  addConnComToActive(node: NodeModel, text: string,  main_nodeIndex: number = 0){
    const parentId = this.activeId.getValue();
    if(parentId === -1)
      return
    
    const nodes = this.getAllVisibleObject(main_nodeIndex);

    nodes.forEach(noder => {
      if(noder.id === parentId){
        if(noder.type === ObjType.Node){
          const noode = noder as NodeModel;
          const index = noode.connections.findIndex((el) => el.obj === node);
          if(index !== -1)
            noode.connections[index].text = text;
        }
      }
    });
    this.refresh()
  }

  /** Adding text to connection node to node
   * @param parent parent instance
   * @param child child instance
   * @param text string 
   */
  addConnCom(parent: NodeModel, child: NodeModel, text: string){
    const VisibleObjects = this.visibleObjects.getValue();
    const index = parent.connections.findIndex(el => el.obj === child);
    if (index >= 0)
      parent.connections[index].text = text;

    this.refresh()
  }

  /** Adding connection node to node
   * 
   */
  addConnToActive(targetId: number, main_nodeIndex = 0) {
    const parentId = this.activeId.getValue();
    if(parentId === -1)
      return
    let parent: VisibleObject[] = [];
    let child: VisibleObject[] = [];

    const nodes = this.getAllVisibleObject(main_nodeIndex);

      nodes.forEach(noder => {
        if(noder.id === parentId){
          parent.push(noder);
        }
        else if(noder.id === targetId){
          child.push(noder);
        }
      });

    if(parent.length > 0 && child.length > 0 && parent[0] !== child[0]){


      if(parent[0].type === ObjType.Node){
        const part = parent[0] as NodeModel;

        if(child[0].type === ObjType.Node){
          const chil = child[0] as NodeModel;
          if(part.connections.findIndex(el => el.obj === chil) < 0 && chil.connections.findIndex(el => el.obj === part) < 0)
            part.connections.push({obj: chil});
        }
        else if(child[0].type === ObjType.Description){
          const chil = child[0] as DescriptionModel;
          if(part.descriptions.findIndex(el => el === chil) < 0)
            part.descriptions.push(chil);
        }
      }
      else if(parent[0].type === ObjType.Description){
        const part = parent[0] as DescriptionModel;

        if(child[0].type === ObjType.Node){
          const chil = child[0] as NodeModel;
          if( chil.descriptions.findIndex(el => el === part) < 0)
            chil.descriptions.push(part);
        }
      }
    }
    this.refresh();
  }

  /** delete connection node to node
   * 
   */
  deleteConnFromActive(targetId: number, main_nodeIndex = 0) {
    console.log('del')
    const parentId = this.activeId.getValue();
    if(parentId === -1)
      return
    
    // no matter ts types errors;
    let parent: VisibleObject[] = [];
    let child: VisibleObject[] = [];

    const nodes = this.getAllVisibleObject(main_nodeIndex);
      nodes.forEach(noder => {
        if(noder.id === parentId){
          parent.push(noder);
        }
        else if(noder.id === targetId){
          child.push(noder);
        }
      });

    if(parent.length > 0 && child.length > 0 && parent[0] !== child[0]){
      if(parent[0].type === ObjType.Description){
        const desc = parent[0] as DescriptionModel;
        if(child[0].type === ObjType.Node){
          const node = child[0] as NodeModel;
          const index =  node.descriptions.indexOf(desc);
          if(index > -1)
            node.descriptions.splice(index, 1);
        }
      }
      else if(child[0].type === ObjType.Description){
        const desc = child[0] as DescriptionModel;
        if(parent[0].type === ObjType.Node){
          const node = parent[0] as NodeModel;
          const index =  node.descriptions.indexOf(desc);
          console.log(node, desc, index)
          if(index > -1)
            node.descriptions.splice(index, 1);
        }
      }
      else if(parent[0].type === ObjType.Node){
        const part = parent[0] as NodeModel;

        if(child[0].type === ObjType.Node){
          const chil = child[0] as NodeModel;
          const index = part.connections.findIndex(el => el.obj === chil);
          if(index > -1){
            part.connections.splice(index, 1);
            this.addNodeWithoutParent(child[0] as NodeModel);
          }
          else{
            const index2 = chil.connections.findIndex(el => el.obj === part);
            if(index2 > -1){
              chil.connections.splice(index2, 1);
              this.addNodeWithoutParent(parent[0] as NodeModel);
            }
          }
        }
      }

    }
    this.refresh();
  }

  /**Delete active node / desctription
   * 
   */
  deleteActive(main_nodeIndex = 0){
    const id = this.activeId.getValue();
    if(id === -1)
      return
    const nodes = this.getAllVisibleObject(main_nodeIndex);
    const main_nodes = this.getNodes(0);

    main_nodes?.forEach((node, index) => {
      if(node.id === id){
        main_nodes.splice(index, 1);
        node.connections.forEach(child => main_nodes.push(child.obj));
        this.activeId.next(main_nodes[0]?.id || -1);
      }
    })

    nodes.forEach(obj => {
      if(obj.type === ObjType.Node){
        const node = obj as NodeModel;
        const index = node.connections.findIndex(el => el.obj.id === id)
        if(index >= 0){
          node.connections[index].obj.connections.forEach(child => main_nodes?.push(child.obj));
          node.connections.splice(index, 1);
          this.activeId.next(obj.id);
        }

        const indexDesc = node.descriptions.findIndex(el => el.id === id)
        if(indexDesc >= 0){
          node.descriptions.splice(indexDesc, 1);
          this.activeId.next(obj.id);
        }
      }
    });
    this.refresh();

  }
  deleteDesc(desc: DescriptionModel, main_nodeIndex = 0){
    const nodes = this.getAllVisibleObject(main_nodeIndex);
    nodes.forEach(obj => {
      if(obj.type === ObjType.Node){
        const node = obj as NodeModel;
        const index = node.descriptions.indexOf(desc);
        if(index >= 0) 
          node.descriptions.splice(index, 1);
      }
    });
    this.refresh();
  }

  refreshNodes(nodes: NodeModel[], index = 0){
    let VO = this.visibleObjects.value;
    VO[index] = [... new Set(nodes)];
    this.visibleObjects.next(VO);
    this.refresh()
  }
}


