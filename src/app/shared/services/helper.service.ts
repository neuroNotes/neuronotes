import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class HelperService {
  canDelete = true;
  constructor() { }

  startTextEditing(){
    this.canDelete = false;
  }
  endTextEditing(){
    this.canDelete = true;
  }

  canIDelete(): boolean{
    return this.canDelete
  }

}
