import { ObjType } from "../obj-type";
import { PositionModel } from "../position-model";

export interface VisibleObjectDTO {
    id: number;
    position: PositionModel;
    type: ObjType;
}
