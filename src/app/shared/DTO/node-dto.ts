import { VisibleObjectDTO } from "./visible-object-dto";

export interface NodeDTO extends VisibleObjectDTO {
    name: string;
    descriptionsIds: number[];
    connectionsIds: {obj: number, text?: string}[];
}
