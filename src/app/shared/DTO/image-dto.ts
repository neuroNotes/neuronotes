export interface ImageDTO {
    file: File;
    id: number;
    width?: number;
    height?: number;
}
