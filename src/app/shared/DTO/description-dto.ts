import { VisibleObjectDTO } from "./visible-object-dto";

export interface DescriptionDTO extends VisibleObjectDTO{
    text: string;
    isNested: boolean;
    imageId: number;
}
