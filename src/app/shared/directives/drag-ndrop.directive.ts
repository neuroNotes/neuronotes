import { Directive, EventEmitter, HostBinding, HostListener, Output } from '@angular/core';
import { ImageTransferModel } from './image-transfer-model';

@Directive({
  selector: '[appDragNDrop]'
})
export class DragNDropDirective {

  constructor() { }
    
  @HostBinding('class.fileover')
  fileOver: boolean = false;

  @Output() fileDropped = new EventEmitter<ImageTransferModel>();

  // Dragover listener
  @HostListener('dragover', ['$event']) onDragOver(evt: DragEvent): void {
    // console.log('dragover',evt);
    evt.preventDefault();
    evt.stopPropagation();
    this.fileOver = true;
  }

  // Dragleave listener
  @HostListener('dragleave', ['$event']) public onDragLeave(evt: DragEvent) {
    // console.log('dragleave',evt);
    evt.preventDefault();
    evt.stopPropagation();
    this.fileOver = false;
  }

  // Drop listener
  @HostListener('drop', ['$event', 'this']) public ondrop(evt: DragEvent, tt: any) {
    // console.log('drop',evt, tt);
    evt.preventDefault();
    evt.stopPropagation();
    this.fileOver = false;
    let files = evt.dataTransfer?.files;
    if (files && files.length > 0 && files.item(0)!== null) {
      this.fileDropped.emit({
        image: files.item(0),
        x: evt.pageX,
        y: evt.pageY
      });
    }
  }
}
