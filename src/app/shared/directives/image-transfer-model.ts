export interface ImageTransferModel {
    image: File | null;
    x: number;
    y: number;
}
