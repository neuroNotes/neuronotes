export interface PositionModel {
    top: number;
    left: number;
}
