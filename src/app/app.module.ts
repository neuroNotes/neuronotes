import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';
import {DragDropModule} from '@angular/cdk/drag-drop'
import { HttpClientModule } from '@angular/common/http';

import { NgxSvgModule } from 'ngx-svg';


import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { NavBarComponent } from './bars/nav-bar/nav-bar.component';
import { ElementsBarComponent } from './bars/elements-bar/elements-bar.component';
import { DetalicsBarComponent } from './bars/detalics-bar/detalics-bar.component';
import { NodeComponent } from './components/node/node.component';
import { WorkPageComponent } from './pages/work-page/work-page.component';
import { MenuPageComponent } from './pages/menu-page/menu-page.component';
import { DesctriptionComponent } from './components/desctription/desctription.component';
import { CommonModule } from '@angular/common';
import { DragNDropDirective } from './shared/directives/drag-ndrop.directive';

@NgModule({
  declarations: [
    AppComponent,
    NavBarComponent,
    ElementsBarComponent,
    DetalicsBarComponent,
    NodeComponent,
    WorkPageComponent,
    MenuPageComponent,
    DesctriptionComponent,
    DragNDropDirective,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    CommonModule,
    NgxSvgModule,
    DragDropModule,
    HttpClientModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
