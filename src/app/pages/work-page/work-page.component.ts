import {DOCUMENT} from "@angular/common";
import { AfterViewInit, Component, ElementRef, HostListener, Inject, OnInit, ViewChild } from '@angular/core';
import { DescriptionModel } from 'src/app/shared/description-model';
import { ImageTransferModel } from 'src/app/shared/directives/image-transfer-model';
import { NodeModel } from 'src/app/shared/node-model';
import { NodeToNodeConnection } from 'src/app/shared/node-to-node-connection';
import { ObjType } from 'src/app/shared/obj-type';
import { PositionModel } from 'src/app/shared/position-model';
import { ImageService } from 'src/app/shared/services/image.service';
import { VisibleObjectManipulationService } from 'src/app/shared/services/visible-object-manipulation.service';
import { VisibleObject } from 'src/app/shared/visible-object';
import 'leader-line';
import { NodeToDescConnection } from "src/app/shared/node-to-desc-connection-model";
import { HelperService } from "src/app/shared/services/helper.service";
import { DataManipulationService } from "src/app/shared/services/data-manipulation.service";


declare let LeaderLine: any;


@Component({
  selector: 'app-work-page',
  templateUrl: './work-page.component.html',
  styleUrls: ['./work-page.component.scss']
})
export class WorkPageComponent implements OnInit, AfterViewInit {
  
  visibleTable: VisibleObject[] = [];
  nodesTable: NodeModel[] = [];
  nodeToNode: NodeToNodeConnection[] =[];
  descesFree: DescriptionModel[] = [];
  descesConns: NodeToDescConnection[] = [];

  textInput: string = '';
  clickedTop: number = 500;
  clickedLeft: number = 500;
  isInputText: boolean = false;

  addingNodeType: ObjType = ObjType.none;
  isFreeNode: boolean = false;

  connTextAdding = false;
  connTextAdd =  '';

  selectedConn?: NodeToNodeConnection;

  lines: {line:any, con: NodeToNodeConnection, used?: boolean}[] = [];
  descLines: {line:any, con: NodeToDescConnection, used?: boolean}[] = [];

  @ViewChild('textInputt', {static: false}) set textInputt(con: ElementRef){
    if(con){
      con.nativeElement.focus();
    }
  }
  @ViewChild('arrowInput', {static: false}) set arrowInput(con: ElementRef){
    if(con){
      con.nativeElement.focus();
    }
  }

  @ViewChild('arrowInput', {static: false}) set textArrowInput(con: ElementRef){
    if(con){
      con.nativeElement.focus();
    }
  }

  @HostListener('window:keyup', ['$event'])
  keyEvent(event: KeyboardEvent) {
    if(event.key == 'Delete' || event.key == 'x') {
      console.log('del start')
      console.log('can delete', this.helperService.canIDelete())
      if(!this.connTextAdd && !this.isInputText && this.helperService.canIDelete()){
        this.removeLines(this.visibleObjectManipulationService.activeId.getValue())
        this.visibleObjectManipulationService.deleteActive();
      }
    }
  }
  

  constructor(
    @Inject(DOCUMENT) private document: Document,
    private visibleObjectManipulationService: VisibleObjectManipulationService, 
    private imageService: ImageService,
    private helperService: HelperService,
    private dataManipulationService: DataManipulationService
    ) {}

  ngOnInit(): void {
    this.visibleTable = this.visibleObjectManipulationService.getAllVisibleObject();
    this.nodesTable = this.getNodes();
    this.nodeToNode = this.getNodeToNode();


    this.visibleObjectManipulationService.visibleObjects.subscribe(objs => {
      this.visibleTable = this.visibleObjectManipulationService.getAllVisibleObject();
      this.nodesTable = this.getNodes();
      this.nodeToNode = this.getNodeToNode();
      this.descesFree = this.getfreeDesces();
      this.descesConns = this.getDescesConns();
    })
  }
  
  ngAfterViewInit(): void {
    this.visibleObjectManipulationService.visibleObjects.subscribe(objs => {
    this.createNodeLines();
    this.createDescLines();
    });
    this.dataManipulationService.isLoading().subscribe(is => {if(is) {
      console.log("reset lines")
      this.lines.forEach(x=>x.line.remove())
      this.descLines.forEach(x=>x.line.remove())
      this.lines = [];
      this.descLines = [];
    }})
  }

  createNodeLines(){
      this.nodeToNode.forEach(con => {
        if(document.querySelector('#node'+ con.twoObj.id+" > #nodeBox") && document.querySelector('#node'+ con.oneObj.id+ " > #nodeBox")){
        let index = this.lines.findIndex((element)=> element.con.oneObj === con.oneObj && element.con.twoObj === con.twoObj);
        if( index == -1 ){
          
          let line = new LeaderLine(
            document.querySelector('#node'+ con.oneObj.id+" > #nodeBox"),
            document.querySelector('#node'+ con.twoObj.id+" > #nodeBox"),
            {middleLabel: LeaderLine.pathLabel('')}
          );
            
          let element: HTMLElement | null=document.body.querySelector(':scope>svg.leader-line:last-of-type');
          element!.style.setProperty('pointer-events', 'auto', 'important');
  
          element?.addEventListener("dblclick",(x: any) => {
            this.showArrowPopUp(line, con);
          });
          this.lines.push({line: line, con: con, used: true});
        }
        else{
          this.lines[index].used = true;
        }
      }
      else {
        // console.error("Brak node", this.nodeToNode, "nodes: 1,2",
        // document.querySelector('#node'+ con.oneObj.id+" > #nodeBox"),
        // document.querySelector('#node'+ con.twoObj.id+" > #nodeBox"),)
      }
      });
  
      this.lines.forEach(function(line, index, object){
        if(!line.used){
          line.line.remove();
          object.splice(index, 1);
        }
        else{
          line.line.position();
          line.used = false;
        }
      })
  }
  

  createDescLines(){
    this.descesConns.forEach(con => {
      if(document.querySelector('#desc'+ con.desc.id+" > #container") && document.querySelector('#node'+ con.node.id+" > #nodeBox")){
      let index = this.descLines.findIndex((element)=> element.con.desc === con.desc && element.con.node === con.node);
      if( index == -1 ){

        let line = new LeaderLine(
          document.querySelector('#node'+ con.node.id+" > #nodeBox"),
          document.querySelector('#desc'+ con.desc.id+" > #container"),
          {startPlug: "disc", endPlug: "disc"}
        );

        this.descLines.push({line: line, con: con, used: true});
      }
      else{
        this.descLines[index].used = true;
      }
    }
    });

    this.descLines.forEach(function(line, index, object){
      if(!line.used){
        line.line.remove();
        object.splice(index, 1);
      }
      else{
        line.line.position();
        line.used = false;
      }
    })
  }

  removeLines(id: number){
    this.lines.forEach((line, index, array) => {
      if(line.con.oneObj.id == id || line.con.twoObj.id == id){
        line.line.remove();
        array.splice(index, 1);
      }
    })
    this.descLines.forEach((line, index, array) => {
      if(line.con.desc.id == id || line.con.node.id == id){
        line.line.remove();
        array.splice(index, 1);
      }
    })
  }

  isShowArrowPopUp: boolean = false;
  tempLine: any;
  tempCon: any;
  arrowPopUpText: string = '';

  showArrowPopUp(line: any, con: NodeToNodeConnection){
    this.isShowArrowPopUp = true;
    this.tempLine = line;
    this.tempCon = con;
    this.helperService.startTextEditing();
  }

  submitArrowPopUp(){
    this.tempLine.setOptions({middleLabel: LeaderLine.pathLabel(this.arrowPopUpText)});
    this.tempCon.connText = this.arrowPopUpText;
    this.hideArrowPop()

  }

  hideArrowPop(){
    this.arrowPopUpText = '';
    this.isShowArrowPopUp = false;
    this.helperService.endTextEditing();
  }

  getNodeToNode(): NodeToNodeConnection[]{
    const cons: NodeToNodeConnection[] = [];
    this.nodesTable.forEach((node, nindex) => {
      node.connections.forEach((connection, index )=> {
        cons.push({
          one: node.position, 
          two: connection.obj.position, 
          connText: connection.text || '', 
          oneObj: node, 
          twoObj: connection.obj
        });
      })
    })
    return cons;
  }

  getNodes(): NodeModel[]{
    const nodes: NodeModel[] = [];
    this.visibleTable.forEach(obj => obj.type == ObjType.Node ? nodes.push(obj as NodeModel) : true)
    return nodes;
  }

  /*
  // getAngle(pos: {one: PositionModel, two: PositionModel}): number{
  //   const a = (pos.two.top - pos.one.top)/(pos.two.left - pos.one.left);
  //   let angle = Math.atan(a);
  //   angle = angle * (180/Math.PI);
  //   if(pos.two.left < pos.one.left)
  //     angle =  (angle > 0) ? angle - 180 : angle + 180;

  //   return angle;
  // }

  // getArowY(pos: {one: PositionModel, two: PositionModel}): number{

  //   let center = (pos.one.top + pos.two.top)/2;

  //   return center;
  // }
  // getArowX(pos: {one: PositionModel, two: PositionModel}): number{
  //   let center = (pos.one.left + pos.two.left)/2 ;
  //   return center;
  // }
*/
 
  getfreeDesces(): DescriptionModel[]{
    const table: DescriptionModel[] = [];
    this.visibleTable.forEach(obj => {
      if(obj.type === ObjType.Description){
        const desc = obj as DescriptionModel;
        if(!desc.isNested)
          table.push(desc);
      }
    });
    return table;
  }

  getDescesConns():NodeToDescConnection[]{
    const table: NodeToDescConnection[] = [];
    this.nodesTable.forEach(node => {
      node.descriptions.forEach(desc => {
        if(!desc.isNested)
          table.push(
            {one: desc.position, desc: desc, node: node , two:node.position}
            );
      })
    })

    // this.descesFree.forEach(desc => {
    //   desc.nodes.forEach(node => table.push(
    //     {one: desc.position, two:node.position}
    //   ))
    // });
    
    return table;
  }

  showInputToNode($event: any){
    this.helperService.startTextEditing();
    this.isInputText = true;
    this.clickedLeft = $event.pageX;
    this.clickedTop = $event.pageY;
    if($event.shiftKey){
      this.addingNodeType = ObjType.Node;
      this.isFreeNode = true;
    }
    else if($event.ctrlKey)
      this.addingNodeType = ObjType.Description;
    else
      this.addingNodeType = ObjType.Node;
  }


  addNode(){
    this.isInputText = false;
    this.helperService.endTextEditing();

    if(this.addingNodeType === ObjType.Node)
      if(!this.isFreeNode)
        this.visibleObjectManipulationService.addNodeToActive(new NodeModel(
          this.textInput, this.visibleObjectManipulationService.nextId, true, {left: this.clickedLeft, top: this.clickedTop} 
        ));
      else{
        this.visibleObjectManipulationService.addNodeWithoutParent(new NodeModel(
          this.textInput, this.visibleObjectManipulationService.nextId, true, {left: this.clickedLeft, top: this.clickedTop}
        ));
        this.isFreeNode = false;
      }
    else if(this.addingNodeType === ObjType.Description)
      this.visibleObjectManipulationService.addDescToActive(new DescriptionModel(
        this.textInput, this.visibleObjectManipulationService.nextId, true, false, {left: this.clickedLeft, top: this.clickedTop} ));
    this.textInput = '';
  }

  hideInput(){
    this.isInputText = false;
    this.textInput = '';
    this.helperService.endTextEditing();

  }

  arrowClicked(conn: NodeToNodeConnection){
    this.selectedConn = conn;
    this.connTextAdd = conn.connText || '';
    this.connTextAdding = true;
  }

  addArrowText(){
    if(this.selectedConn)
      this.visibleObjectManipulationService.addConnCom(this.selectedConn.oneObj, this.selectedConn.twoObj, this.connTextAdd);
    this.connTextAddingClose();
  }

  connTextAddingClose(){
    this.connTextAdding = false;
    this.selectedConn = undefined;
    this.connTextAdd = '';
  }

  addImageDesc(event: ImageTransferModel){
    this.visibleObjectManipulationService.addDescToActive(
      new DescriptionModel('', this.visibleObjectManipulationService.nextId, true, false, {left: event.x, top: event.y}, this.imageService.makeImage(event.image))
    )
  }


  isGrabbing: boolean = false;
  posGrab = { top: 0, left: 0, x: 0, y: 0 };

  startGrabbing($event:MouseEvent){
    this.isGrabbing = true;
    this.posGrab = {
      left: window.scrollX,
      top: window.scrollY,
      x: $event.clientX,
      y: $event.clientY,
  };
  }
  grabbing($event:MouseEvent){
    if(this.isGrabbing){
      const dx = $event.clientX - this.posGrab.x;
      const dy = $event.clientY - this.posGrab.y;
  
      // Scroll the element
      const y = this.posGrab.top - dy; // y
      const x = this.posGrab.left - dx; // x
      window.scroll(x , y);
    }
  }
  endGrabbing(){
    this.isGrabbing = false;
  }


  nodeInited(node: NodeModel){
    // console.log(this.nodesTable.indexOf(node), node.name, this.nodesTable.length)
    // if(this.nodesTable.indexOf(node) == this.nodesTable.length - 2 || this.nodesTable.indexOf(node) == this.nodesTable.length - 1){
      this.createNodeLines();
      this.createDescLines();
    // }
  }

  drag(event: any){
      // this.createDescLines();
      this.descLines.forEach(l => l.line.position());
    // this.createDescLines();

    this.lines.forEach(l => l.line.position());
    console.log("drag", this.descLines);
  }

  descInited(node: DescriptionModel){
    // if(this.descesFree.indexOf(node) == this.descesFree.length - 1){
      this.createDescLines();
    }
  // }
}
