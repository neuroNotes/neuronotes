import { CdkDragDrop, CdkDragEnd, CdkDragMove, moveItemInArray } from '@angular/cdk/drag-drop';
import { AfterViewInit, Component, ElementRef, EventEmitter, Input, OnInit, Output, ViewChild } from '@angular/core';
import { DescriptionModel } from 'src/app/shared/description-model';
import { NodeModel } from 'src/app/shared/node-model';
import { VisibleObjectManipulationService } from 'src/app/shared/services/visible-object-manipulation.service';

@Component({
  selector: 'app-node',
  templateUrl: './node.component.html',
  styleUrls: ['./node.component.scss']
})
export class NodeComponent implements OnInit, AfterViewInit {

  @Input() main_node: NodeModel | undefined;

  @Output() drag: EventEmitter<any> = new EventEmitter();
  @Output() inited: EventEmitter<any> = new EventEmitter();


  
  descesNested: DescriptionModel[] = []
  textNested: string = '';

  isActive = false;

  isAddNested: boolean = false;

  @ViewChild('descInput', {static: false}) set
  textInputt(con: ElementRef){
    if(con){
      con.nativeElement.focus();
    }
  }
  @ViewChild('node', {static: false}) node?: ElementRef;

  constructor(private visibleObjectManipulationService: VisibleObjectManipulationService) { }

  ngOnInit(): void {
    this.visibleObjectManipulationService.activeId.subscribe(x => {if(x === this.main_node?.id) this.isActive = true; else this.isActive = false;});
    this.visibleObjectManipulationService.visibleObjects.subscribe(()=>this.getDesces());
 
  }
  ngAfterViewInit()	{
    if(this.node)
      this.node.nativeElement.style.transform = " translate(-50%, -50%)";
    this.inited.emit(this.main_node);
  }

  clicked(event: MouseEvent): void {
    if(event.shiftKey)
    this.visibleObjectManipulationService.deleteConnFromActive((this.main_node && this.main_node.id)|| 0);
    else if(event.ctrlKey)
      this.visibleObjectManipulationService.addConnToActive((this.main_node && this.main_node.id)|| 0);
    else
      this.visibleObjectManipulationService.changeActiveId((this.main_node && this.main_node.id)|| 0, this.main_node);
  }
  getDesces(){
    this.descesNested = [];
    this.main_node?.descriptions.forEach((desc)=>{
      if(desc.isNested)
        this.descesNested.push(desc);
    })
  }
  dragEnded($event: CdkDragEnd) {
    if(this.main_node){
      this.main_node.position.left += $event.distance.x;
      this.main_node.position.top += $event.distance.y;
    }
    this.visibleObjectManipulationService.refresh();
    $event.source._dragRef.reset();
    if(this.node)
      this.node.nativeElement.style.transform = " translate(-50%, -50%)";

  }

  addNested(){
    this.visibleObjectManipulationService.addDescToActive(new DescriptionModel(
      this.textNested,
      this.visibleObjectManipulationService.nextId,
      true,
      true,
      {left: 0, top: 0}
    ));
    this.textNested = '';
  }
  showDescInput(){
    this.isAddNested = true;
  }

  dropDesc(event: CdkDragDrop<DescriptionModel[]>) {
    moveItemInArray(this.main_node?.descriptions || [], event.previousIndex, event.currentIndex);
    this.visibleObjectManipulationService.refresh();
  }


  isMouseOverDesc = false;
  descWithMouse: DescriptionModel | undefined;

  mouseOverDesc(desc: DescriptionModel){
    this.isMouseOverDesc = true;
    this.descWithMouse = desc;
    console.log('in');
  }

  mouseLeaveDesc(){
    this.isMouseOverDesc = false;
    console.log('over');
  }
  delDesc(desc: DescriptionModel){
    this.visibleObjectManipulationService.deleteDesc(desc);
  }


}