import { CdkDragEnd, CdkDragMove } from '@angular/cdk/drag-drop';

import { Component, ElementRef, EventEmitter, Input, OnInit, Output, ViewChild } from '@angular/core';
import { DescriptionModel } from 'src/app/shared/description-model';
import { VisibleObjectManipulationService } from 'src/app/shared/services/visible-object-manipulation.service';

@Component({
  selector: 'app-desctription',
  templateUrl: './desctription.component.html',
  styleUrls: ['./desctription.component.scss']
})
export class DesctriptionComponent implements OnInit {

  @Input() main_node: DescriptionModel | undefined;

  @Output() drag: EventEmitter<any> = new EventEmitter();
  @Output() inited: EventEmitter<any> = new EventEmitter();

  
  @ViewChild('image', {static: false}) image: ElementRef | undefined; 
  @ViewChild('node', {static: false}) node?: ElementRef;

  imgHeight: number = 300;
  startHeight: number | undefined;

  active: boolean = false;

  hasImage = false;
  imageSrc: string = '';

  constructor(private visibleObjectManipulationService: VisibleObjectManipulationService) { }

  ngOnInit(): void {
    this.visibleObjectManipulationService.activeId.subscribe(id =>this.active = this.main_node?.id === id );
    this.visibleObjectManipulationService.visibleObjects.subscribe(()=> {
      this.hasImage = !(this.main_node?.image === null);
      this.getImageSrc();
    });
  }

  ngAfterViewInit()	{
    if(this.node)
      this.node.nativeElement.style.transform = " translate(-50%, -50%)";
    this.inited.emit(this.main_node);
  }


  clicked(event: any){
    if(event.shiftKey)
    this.visibleObjectManipulationService.deleteConnFromActive((this.main_node && this.main_node.id)|| 0);
    else if(event.ctrlKey)
      this.visibleObjectManipulationService.addConnToActive((this.main_node && this.main_node.id)|| 0);
    else
      this.visibleObjectManipulationService.changeActiveId((this.main_node && this.main_node.id)|| 0, this.main_node);
  }

  dragEnded($event: CdkDragEnd) {
    if(this.main_node){
      this.main_node.position.left += $event.distance.x;
      this.main_node.position.top += $event.distance.y;
    }
    this.visibleObjectManipulationService.refresh();
    $event.source._dragRef.reset();
    if(this.node)
      this.node.nativeElement.style.transform = " translate(-50%, -50%)";
  }

  getImageSrc(){
    const reader = new FileReader();
    reader.onloadend = () => {
      this.imageSrc = reader.result + '';
    }
    if(this.main_node && this.main_node.image!== null)
      reader.readAsDataURL(this.main_node.image.image);
  }

  sizeBallDragMoved(e:CdkDragMove){
    this.startHeight = this.image?.nativeElement.height;
    this.imgHeight = (this.startHeight || 0) + (e.distance.y > 0 ? Math.sqrt(e.distance.y): -Math.sqrt(Math.abs(e.distance.y)));

    e.source._dragRef.reset();
  }

  draging(){
    this.drag.emit({});
  }
}
