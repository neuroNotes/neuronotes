import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DetalicsBarComponent } from './detalics-bar.component';

describe('DetalicsBarComponent', () => {
  let component: DetalicsBarComponent;
  let fixture: ComponentFixture<DetalicsBarComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DetalicsBarComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DetalicsBarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
