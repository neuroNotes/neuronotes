import { Component, Input, OnInit } from '@angular/core';
import { DescriptionModel } from 'src/app/shared/description-model';
import { NodeModel } from 'src/app/shared/node-model';
import { DataManipulationService } from 'src/app/shared/services/data-manipulation.service';
import { VisibleObjectManipulationService } from 'src/app/shared/services/visible-object-manipulation.service';

@Component({
  selector: 'app-elements-bar',
  templateUrl: './elements-bar.component.html',
  styleUrls: ['./elements-bar.component.scss']
})
export class ElementsBarComponent implements OnInit {

  name:string = "";
  desc:string = "";
  checkingPos: boolean = false;
  connCom = '';
  isNested = true;
  addingDesc = false;

  connComActive = false;

  constructor(private visibleObjectManipulationService: VisibleObjectManipulationService, private dataManipulationService: DataManipulationService) { }

  ngOnInit(): void {
    this.visibleObjectManipulationService.getSecondSelect().subscribe((node)=> {
      this.connComActive = false;
      this.visibleObjectManipulationService.addConnComToActive(node as NodeModel, this.connCom)
    })
  }

  onAdd(){
    this.checkingPos = true;
  }

  onConnAdd(){
    this.visibleObjectManipulationService.secondSelect();
    this.connComActive = true;
  }

  onDesc(){
      if(this.isNested){
        this.visibleObjectManipulationService.addDescToActive(new DescriptionModel(
          this.desc,
          this.visibleObjectManipulationService.nextId,
          true,
          this.isNested,
          {left: 0, top: 0}
        ));
      }
      else{
        this.checkingPos = true;
        this.addingDesc = true;
      }
  }

  positioned($event: MouseEvent){
    this.checkingPos = false;
    if(this.addingDesc){
      this.visibleObjectManipulationService.addDescToActive(new DescriptionModel(
        this.desc,
        this.visibleObjectManipulationService.nextId,
        true,
        this.isNested,
        {left: $event.pageX, top: $event.pageY}
      ));
      this.addingDesc = false;
    }
    else{
    this.visibleObjectManipulationService.addNodeToActive(new NodeModel(
      this.name, this.visibleObjectManipulationService.nextId, true, {left: $event.pageX, top: $event.pageY} 
      
    ))
    }
  }

  save(){
    console.log('Save');
    this.dataManipulationService.saveVisibleObjects().subscribe(console.log);
  }

  load(){
    console.log('Load');
    this.dataManipulationService.loadAll();
  }
}
