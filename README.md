# NeuroNotetes

Projekt stworzony aby ułatwić tworzenie map myśli, czyli map powiązań między elementami

>
> ## Spis Treści
>
> - [Start](#start)
> - [Użytkowanie](#użytkowanie)
> - [Błędy i propozycje](#zgłaszanie-błędów-i-pomysłów-rozwoju)
> - [Technologie](#technologie)
> - [Pliki i katalogi](#struktura-projektu)

## Start

Uruchomienie projektu przez `npm start` lub `ng serve`, uprzednio należy pobrać potrzebne biblioteki przez `npm install`

Uzyskanie funkcji zapisu i wczytywania wymaga pobrania projektu `NeuroNotes backend`, uruchomienia go i skonfigurowania bazy danych mySql

## Użytkowanie

*Pasek boczny po prawej stronie zawiera narzędzia do debugowania*
Aplikacja umożliwia stworzenie dwóch typów obiektów. Są to Węzły i Opisy.

- Każdy obiekt może stać się aktywny poprzez dwukrotne go kliknięcie.
- Każdy obiekt po kliknięciu delete lub x zostanie usunięty
- po dwukrotnym kliknięciu z klawisze shift innego węzła lub opisu zostanie usunięte jego powiązanie z aktywnym
- po dwukrotnym kliknięciu z klawisze ctrl innego węzła lub opisu zostanie dodane powiązanie go z aktywnym
- każdy obiekt da się przesuwać poprzez przeciągnięcie go myszką
- po planszy można się poruszać poprzez scroll lub przeciągnij jej

### Węzły

- Aby stworzyć węzeł kliknij dwukrotnie na pusty obszar planszy i wpisz jego nazwę, zostanie automatycznie powiązany z aktywnym węzłem lub opisem
- klikając przycisk plus można stworzyć wewnętrzne opisy

#### Wewnętrzny opis węzła

- wewnętrznymi opisami da się manipulować tylko w aktywnym węźle
- aby usunąć wewnętrzny opis należy najechać na niego i kliknąć -
- można zmieniać kolejność wewnętrznych opisów poprzez ich przesuwanie

### Opisy

Są dwa rodzaje opisów. Te ze zdjęciem i tekstów. Opisy nie posiadają opisów wewnętrznych ani strzałek. Są po prostu odnośnikami do węzłów

#### Opisy z tekstem

- aby utworzyć opis z tekstem należy kliknąć dwukrotnie z klawiszem ctrl w puste pole i nadać mu nazwę i kliknąć enter
- opis zostanie automatycznie powiązany z aktywnym węzłem
- usuwanie przez klawisz delete lub x

#### Opisy ze zdjęciami

- aby utworzyć opis ze zdjęciem należy przeciągnąć zdjęcie w puste miejsce na planszy
- czerwona kropka w prawym dolnym rogu pozwala manipulować wielkością zdjęcia

### Połączenia

- Karzde połączenie węzeł do węzła może mieć swój podpis
- aby dodać podpis należy kliknąć dwukrotnie połączenie

### Zapisywanie i wczytywanie

- po skonfigurowaniu projektu `NeuroNotes backend` będzie możliwe zapisanie projektu poprzez przycisk "zapisz" w bocznym menu i go wczytania poprzez przycisk "wczytaj"
*funkcja jest dalej rozwijana*

## Zgłaszanie Błędów i pomysłów rozwoju

- Stwórz nowy issue na gitlab w projekcie, opisz błąd, jego lokalizację oraz w jakich okolicznościach zaistniał, zmień typ z issue na incydent
- Pomysły zgłoś w podobny sposób tylko oznacz go jako issue

## Technologie

- Angular
- LeaderLine

## Struktura Projektu

katalog src/app zawiera:

- bars: odpowiada za paski, aktualnie rozwijany jest elements bar jako pasek z narzędziami do debugu
- pages: zawiera wszystkie komponenty, pojawiające się w routerze jako nadrzędne strony
  - work-page jest zawiera cały kod odnośnie wyświetlania i manipulacją obiektami
  - main-page *jest w fazie rozwoju*
- components: zawiera komponenty używane w pages
- shared: zawiera modele danych używanych w całej aplikacji
  - DTO zawiera modele danych na wejściu i wyjściu do backendu
  - directives zawiera dyrektywy, aktualnie odpowiedzialną za drag and drop files, używanych do tworzenia opisów ze zdjęciami
  - services zawiera wszystkie serwisy oprogramowania
    - data-manipulation odpowiada za komunikacje z backend
    - helper odpowiada za pomocnicze funkcje takie jak sprawdzanie, czy aktualnie jest otwarte pole tekstowe
    - image odpowiada za przesyłanie i obracanie obrazów
    - visible-object-manipulation jest sercem oprogramowania i odpowiada za wszystkie procesy związane ze zmianami w obiektach takich jak węzły i opisy

w pliku environment zawarte są linki do api i stałe wykorzystywane w aplikacji
